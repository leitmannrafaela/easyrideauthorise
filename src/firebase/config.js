import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AAAAaq9A6wg:APA91bGOrYZBomW-GC20hgR9kPkmAlMPOp_3diZxgP2An4-e2pIG5qA22QUSmkBKQRi48tflQvMsTUpEoh-DNdK0jL3Vb32x-YKo6rLcD7mtB7OMeHIclylld3oB7qGHhLgKgZPOz1xL',
  authDomain: 'your-auth-domain-b1234.firebaseapp.com',
  databaseURL: 'https://easyride.firebaseio.com',
  projectId: 'easyride-6fc0e',
  storageBucket: 'easyride-6fc0e.appspot.com',
  messagingSenderId: '458206800648',
  appId: '1:458206800648:android:71ea84b2f9f0040fae5abd',
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };